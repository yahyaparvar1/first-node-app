const request = require("request");

const forecast = (lat, long, callback) => {
  const url =
    "http://api.weatherstack.com/current?access_key=30175132d057de3201ab19fa5bd32f6d&query=" +
    long +
    "," +
    lat;

  request({ url: url, json: true }, (error, response) => {
    if (error) {
      callback("Unable to connect to weather service!", undefined);
    } else if (response.body.error) {
      callback("Unable to find location", undefined);
    } else {
      //   console.log(
      //     response.body.current.weather_descriptions[0] +
      //       ". It is currently " +
      //       response.body.current.temperature +
      //       " degress out."
      //   );
      callback(undefined, response.body.current.temperature);
    }
  });
};
module.exports = forecast;
